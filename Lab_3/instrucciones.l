%{
#include <stdio.h>
#include "y.tab.h" 
%}

separador ([ \t""])+
letra [a-zA-Z]
digito [0-9]
espacio [:blank:]+
identificador {letra}({letra}|{digito})*
textPlano ({letra}|{digito})*{espacio}
constEntera {digito}({digito})*
%%

{separador}
"printf"  {return (PRT);}
"scanf"  {return (SCF);}
"//" {return (TRO);}
{constEntera} {return (CONSTENTERA);}
":=" {return (OPAS);}
"+"  {return (MAS);}
"*"  {return (POR);}
"/"  {return (DIV);}
{identificador} {return (IDENTIFICADOR);}
"(" 	 {return (APAR);}
"'" 	 {return (COM);}
")" 	 {return (CPAR);}
\n  	 {return (NL);}
{textPlano} {return (TP);}
.   ECHO;
%%
