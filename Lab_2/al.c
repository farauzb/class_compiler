#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#define MAX 40
#define KGRN  "\x1B[32m"
#define KNRM  "\x1B[0m"
	//PROTOTIPOS
	int analiza(char *);
	int estado_1(int, char *, char *);
	int estado_2(int, char *, char *);
	int estado_3(int, char *, char *);
	int estado_4(int, char *, char *);
	//PROTOTIPOS

void main(int argc, char const *argv[])
{
	int resp;char *cadena = (char *)malloc(MAX*sizeof(char));
	int bandera;
//considerar ciclo repetitivo
	system("clear");
	printf("\tBienvenido al Analizador Lexico.\n\n\n");
		printf("Dame una cadena: ");
		printf(KGRN);scanf("%[^\n]",cadena);printf(KNRM);
		bandera = analiza(cadena);
		if (bandera!=1){
			printf("\n\n\t\tERROR, LA CADENA INGRESADA ES INVALIDA.\n");
		}
//considerar ciclo repetitivo
	//system("clear");
}// End main
/*#################################################################################*/
int estado_4(int pos, char *entrada, char * temp){ //ESTADO SI LEE A
	int band;
		if(*(entrada + pos) == 'b'){ // valida para estar seguro que la ultima posicion sea b
			printf("\n\n\t\tLA CADENA: %s ES ACEPTADA.",temp);
			band = 1;
			if(*(entrada + pos+1)== 'a'){
				printf("\n");
				estado_1(pos,entrada,temp);
			}else{
					if(entrada[pos+1]!='\0'){
						printf("\n");
						estado_2(pos,entrada,temp);
					}else{
						printf("\n\n\nPrograma finalizado...\n");
						getchar();	
					}
			}
		}else{
				printf("\n\n\t\tERROR en el analicis!\n");
				band=0;
		}
	return band;
}//end estado4
/*#################################################################################*/
int estado_3(int pos, char *entrada, char * temp){ //ESTADO SI LEE A
	pos +=1;int band=0;
	printf("ESTADO #3\n");
	while(entrada[pos]!='\0'){
		if(*(entrada + pos) == 'a'){
			strcat(temp, "a"); //concateno el nuevo allasgo
			printf("\tLee e_3:'%c'\n",temp[pos]);
		band = 	estado_1(pos,entrada,temp);
		}else{
			if(*(entrada + pos) == 'b'){
				strcat(temp, "b"); //concateno el nuevo allasgo
				printf("\tLee e_3:'%c'\n",temp[pos]);
			band = 	estado_4(pos,entrada,temp);
			}else{
				printf("\n\n\t\tERROR en el analicis!\n");
				break;
			}	
		}
		break;
	}//while
	return band;
}//end estado3
/*#################################################################################*/
int estado_1(int pos, char *entrada, char * temp){ //ESTADO SI LEE A
	pos +=1; int band=0;
	printf("ESTADO #1\n");
	if(entrada[pos]!='\0'){
		if(*(entrada + pos) == 'a'){
			strcat(temp, "a"); //concateno el nuevo allasgo
			printf("\tLee e_1:'%c'\n",temp[pos]);
			band = estado_1(pos,entrada,temp);
		}else{
			if(*(entrada + pos) == 'b'){
				strcat(temp, "b"); //concateno el nuevo allasgo
				printf("\tLee e_1:'%c'\n",temp[pos]);
			band = 	estado_3(pos,entrada,temp);
			}else{
				printf("\n\n\t\tERROR en el analicis!\n");
				//break; 
			}
		}
		//break;
	}//while
	else
		printf("\n\n\t\tERROR en el analicis!\n");
	return band;
}//end estado1
/*#################################################################################*/
int estado_2(int pos, char *entrada, char *temp){
	pos +=1; int band=0;
	printf("ESTADO #2\n");
	if(entrada[pos]!='\0'){
		if(*(entrada + pos) == 'b'){
			strcat(temp, "b"); //concateno el nuevo allasgo
			printf("\tLee e_2:'%c'\n",temp[pos]);
			band = estado_2(pos,entrada,temp);
		}else{
			if(*(entrada + pos) == 'a'){
				strcat(temp, "a"); //concateno el nuevo allasgo
				printf("\tLee e_2:'%c'\n",temp[pos]);
			band = 	estado_1(pos,entrada,temp);
				//break; //valida al estado1 si es A
			}else{
				printf("\n\n\t\tERROR en el analicis!\n");
				//break;
			}
		}
		//break;
	}//while
	else
		printf("\n\n\t\tERROR en el analicis!\n");
	return band;
}//end estado2
/*#################################################################################*/
int analiza(char *entrada){
	int band=0;
	printf("ESTADO #0\n");
	char *temp = (char *)malloc(20*sizeof(char));
	if(*(entrada)=='a'){
		strcpy((temp), "a");
		printf("\tLee e_0:'%c'\n",*(temp));
		band = estado_1(0,entrada,temp);
	}else{
		if(*(entrada)=='b'){
			strcpy((temp), "b");
			printf("\tLee e_0:'%c'\n",*(temp));
		band = 	estado_2(0,entrada,temp);
		}else{
			printf("\n\n\t\tERROR en el analicis!\n");
		}
	}//end else	
	return band;
}//fin metodo


 